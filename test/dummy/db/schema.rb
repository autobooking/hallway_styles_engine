# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_04_112346) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "station_styles", force: :cascade do |t|
    t.bigint "station_id"
    t.bigint "style_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["station_id", "style_id"], name: "index_station_styles_on_station_id_and_style_id", unique: true
    t.index ["station_id"], name: "index_station_styles_on_station_id"
    t.index ["style_id"], name: "index_station_styles_on_style_id"
  end

  create_table "style_photos", force: :cascade do |t|
    t.bigint "style_id"
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "brand_id"
    t.index ["brand_id"], name: "index_style_photos_on_brand_id"
    t.index ["style_id"], name: "index_style_photos_on_style_id"
  end

  create_table "style_synonym_translations", force: :cascade do |t|
    t.bigint "style_synonym_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["locale"], name: "index_style_synonym_translations_on_locale"
    t.index ["style_synonym_id"], name: "index_style_synonym_translations_on_style_synonym_id"
  end

  create_table "style_synonyms", force: :cascade do |t|
    t.string "name"
    t.text "permitted_countries", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "style_synonyms_styles", id: false, force: :cascade do |t|
    t.bigint "style_id", null: false
    t.bigint "style_synonym_id", null: false
  end

  create_table "style_translations", force: :cascade do |t|
    t.bigint "style_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "slug"
    t.string "tooltip"
    t.text "description"
    t.index ["locale"], name: "index_style_translations_on_locale"
    t.index ["style_id"], name: "index_style_translations_on_style_id"
  end

  create_table "styles", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "tooltip"
    t.text "description"
    t.text "permitted_countries", default: [], array: true
    t.boolean "main", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
