# frozen_string_literal: true

require 'test_helper'

class HallwayStyles::Test < ActiveSupport::TestCase
  test 'truth' do
    assert_kind_of Module, HallwayStyles
  end
end
