# frozen_string_literal: true

module HallwayStyles
  class Engine < ::Rails::Engine
    # isolate_namespace HallwayStyles

    initializer :append_migrations do |app|
      unless app.root.to_s.match(root.to_s) || app.config.paths['db/migrate'].include?('db_terrasoft/migrate')
        config.paths['db/migrate'].expanded.each do |expanded_path|
          app.config.paths['db/migrate'] << expanded_path
        end
      end
    end

    decorators = HallwayStyles::Engine.root.join 'app/decorators'
    config.to_prepare do
      Rails.autoloaders.main.ignore(decorators)
      Dir.glob("#{ decorators }/**/*_decorator.rb").each do |decorator|
        load decorator
      end
    end

    active_admin = HallwayStyles::Engine.root.join 'app/admin'
    initializer 'hallway_styles', after: :load_config_initializers do
      Rails.autoloaders.main.ignore(active_admin)
      ActiveAdmin.application.load_paths << active_admin.to_s
    end
  end
end
