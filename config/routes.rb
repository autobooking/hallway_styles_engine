# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api, format: :json do
    resources :stations, only: [] do
      resources :styles, only: :index, controller: :station_styles
    end
  end

  namespace :station_admin do
    resources :stations, only: [] do
      resources :station_styles, only: [:index, :create, :destroy]
    end

    resources :styles, only: [:index, :edit, :update, :destroy] do
      resources :manage_style_synonyms, only: [:create, :destroy]
    end

    resources :style_synonyms, only: [:index, :edit, :update] do
      resources :manage_styles, only: [:create, :destroy]
    end
  end
end
