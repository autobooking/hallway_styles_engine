# frozen_string_literal: true

require_relative '../models/style_photo'

if defined?(ActiveAdmin)
  ActiveAdmin.register StylePhoto do
    menu parent: 'styles', if: proc {
      Settings.use_styles &&
        current_admin.can?(:manage, StylePhoto)
    }

    permit_params :style_id, :file, :alt

    index download_links: false do
      selectable_column
      id_column
      column :file do |photo|
        image_tag(photo.file.thumb.url) if photo.file.present?
      end
      actions
    end

    show do
      attributes_table do
        row :id
        row :file do |photo|
          image_tag(photo.file.url) if photo.file.present?
        end
        row :alt
        row :created_at
        row :updated_at
      end
    end
  end
end
