# frozen_string_literal: true

require_relative '../models/style_synonym'

if defined?(ActiveAdmin)
  ActiveAdmin.register StyleSynonym do
    menu parent: 'styles', if: proc {
      Settings.use_styles &&
        current_admin.can?(:manage, StyleSynonym)
    }

    config.filters = false

    permit_params :translatable_columns_values, permitted_countries: []

    index download_links: false do
      selectable_column
      id_column
      column :name
      actions
    end

    show do
      attributes_table do
        row :id
        row :name
        row :permitted_countries
        row :created_at
        row :updated_at
      end

      panel 'Translations' do
        table_for resource.translations do
          column :locale
          column :name
        end
      end
    end

    form do |f|
      f.inputs 'Style Details' do
        f.input :translatable_columns_values, as: :json, label: 'Name'
        f.input :permitted_countries,
                as: :select,
                multiple: true,
                collection: AdminPanel::PermittedCountries.new(f.object, current_admin).countries,
                include_hidden: false
      end

      f.actions
    end
  end
end
