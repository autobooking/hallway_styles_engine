# frozen_string_literal: true

require_relative '../models/station_style'

if defined?(ActiveAdmin)
  ActiveAdmin.register StationStyle do
    menu parent: 'Stations', if: proc {
      Settings.use_styles &&
        current_admin.can?(:manage, StationStyle)
    }

    config.filters = false

    permit_params :station_id, :style_id

    index download_links: false do
      selectable_column
      id_column
      column :station
      column :style
      actions
    end

    form do |f|
      f.inputs 'StationStyle' do
        f.input :station, as: :select, collection: Station.all
        f.input :style, as: :select, collection: Style.all
      end

      f.actions
    end
  end
end
