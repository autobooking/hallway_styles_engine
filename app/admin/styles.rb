# frozen_string_literal: true

require_relative '../models/style'

if defined?(ActiveAdmin)
  ActiveAdmin.register Style do
    menu parent: 'styles', if: proc {
      Settings.use_styles &&
        current_admin.can?(:manage, Style)
    }

    config.filters = false

    permit_params :translatable_columns_values, :main, permitted_countries: [],
                  style_photos_attributes: [:id, :file, :alt, :_destroy]

    index download_links: false do
      selectable_column
      id_column
      column :name
      column :slug
      column :main
      column :photo do |style|
        bullet_list(style.style_photos)
      end
      actions
    end

    show do
      attributes_table do
        row :id
        row :name
        row :slug
        row :tooltip
        row :description
        row :permitted_countries
        row :main
        row :created_at
        row :updated_at
      end

      panel 'Translations' do
        table_for resource.translations do
          column :locale
          column :name
          column :slug
          column :tooltip
          column :description
        end
      end

      panel 'Photos' do
        table_for resource.style_photos do
          column :file do |photo|
            image_tag(photo.file.thumb.url) if photo.file.present?
          end
          column :show do |photo|
            link_to 'Show', admin_panel_style_photo_path(photo)
          end
        end
      end
    end

    form do |f|
      f.semantic_errors *f.object.errors.keys
      f.inputs 'Style Details' do
        f.input :translatable_columns_values, as: :json, label: 'Name'
        f.input :main
        f.input :permitted_countries,
                as: :select,
                multiple: true,
                collection: AdminPanel::PermittedCountries.new(f.object, current_admin).countries,
                include_hidden: false
        f.has_many :style_photos, allow_destroy: true do |style_photo|
          style_photo.input :file, as: :file, hint: photo_hint(style_photo.object)
          style_photo.input :alt
        end
      end

      f.actions
    end
  end
end
