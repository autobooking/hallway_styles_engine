# frozen_string_literal: true

module ActiveAdmin
  module ViewsHelper
    include ActionView::Context

    def bullet_list(photos)
      content_tag(:ul) do
        photos.collect do |photo|
          next unless photo.file.present?

          concat(
            content_tag :li, image_tag(photo.file.thumb.url)
          )
        end
      end
    end

    def photo_hint(photo)
      if photo.file.present?
        image_tag(photo.file.thumb.url)
      else
        content_tag(:span, 'No chosen pictures yet')
      end
    end
  end
end
