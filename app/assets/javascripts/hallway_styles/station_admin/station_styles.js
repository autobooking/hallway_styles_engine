jQuery(document).ready(function() {
  $('#station-styles-datatable').dataTable({
    "processing": true,
    "serverSide": true,
    "pageLength": 25,
    "lengthChange": false,
    "ajax": {
      "url": $('#station-styles-datatable').data('source')
    },
    "columns": [
      { "data": "id" },
      { "data": "name" },
      { "data": "delete", "searchable": false, "orderable": false }
    ]
  });
});
