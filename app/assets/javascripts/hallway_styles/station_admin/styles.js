jQuery(document).ready(function() {
  $('#styles-datatable').dataTable({
    "processing": true,
    "serverSide": true,
    "pageLength": 25,
    "lengthChange": false,
    "ajax": {
      "url": $('#styles-datatable').data('source')
    },
    "columns": [
      { "data": "id" },
      { "data": "name" },
      { "data": "in_my_country", "searchable": false, "orderable": false },
      { "data": "edit", "searchable": false, "orderable": false },
      { "data": "delete", "searchable": false, "orderable": false }
    ]
  });
});
