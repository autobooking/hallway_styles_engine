# frozen_string_literal: true

class StyleStyleSynonym < ApplicationRecord
  belongs_to :style

  belongs_to :style_synonym
end
