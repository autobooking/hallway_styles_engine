# frozen_string_literal: true

require 'globalize'

class StyleSynonym < ApplicationRecord
  include TranslationsConcern

  has_many :style_style_synonyms, dependent: :destroy

  has_many :styles, through: :style_style_synonyms

  translates :name

  scope :with_country, ->(code) { where('? = ANY(permitted_countries)', code) }
end
