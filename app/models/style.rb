# frozen_string_literal: true

require 'globalize'

class Style < ApplicationRecord
  include NormalizeSlugConcern

  translates :name, :slug, :tooltip, :description

  accepts_nested_attributes_for :translations

  extend FriendlyId

  friendly_id :slug_candidates, use: [:slugged, :globalize]

  def slug_candidates
    [
      :name,
      [:name, :id]
    ]
  end

  searchkick word_start: [:name]

  include TranslationsConcern
  include ElasticsearchGlobalizeConcern

  has_many :style_photos, dependent: :destroy

  has_many :style_style_synonyms, dependent: :destroy

  has_many :style_synonyms, through: :style_style_synonyms

  has_many :order_styles, dependent: :destroy

  has_many :orders, through: :order_styles

  has_many :station_styles, dependent: :destroy

  has_many :stations, through: :station_styles

  has_many :seo_blocks, dependent: :nullify

  accepts_nested_attributes_for :style_photos, allow_destroy: true

  scope :with_country, ->(code) { where('? = ANY(permitted_countries)', code) }

  scope :with_translations_and_photos, -> { includes(:translations, :style_photos) }

  validate :check_max_photos, on: [:create, :update]

  def to_param
    # From rails
    id&.to_s
  end

  def search_data
    {
      name: name,
      slug: slug,
      permitted_countries: permitted_countries,
      translations: translatable_columns_values
    }.merge(translated_attributes_for_elasticsearch)
  end

  def add_country(code)
    permitted_countries << code if permitted_countries.exclude?(code)

    save!

    reindex
  end

  def remove_country(code)
    permitted_countries.delete(code) if permitted_countries.include?(code)

    save!

    reindex
  end

  def check_max_photos
    errors.add(:base, 'must not contain more than 3 photos') if style_photos.size > 3
  end

  def should_generate_new_friendly_id?
    globalize.fetch_without_fallbacks(:slug).blank? && globalize.fetch_without_fallbacks(:name).present?
  end

  def normalize_friendly_id(text)
    text.to_slug.normalize(
      transliterations: BabosaTransliterator.get_transliterator(
        FakeFallback.get_locale(Globalize.locale)
      )
    ).to_s.downcase.dasherize
  end
end
