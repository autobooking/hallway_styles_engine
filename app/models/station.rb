# frozen_string_literal: true

if !Object.const_defined?('Station')
  class Station < ApplicationRecord
    has_many :station_styles, dependent: :destroy

    has_many :styles, through: :station_styles
  end
end
