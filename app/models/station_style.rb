# frozen_string_literal: true

class StationStyle < ApplicationRecord
  belongs_to :station

  belongs_to :style
end
