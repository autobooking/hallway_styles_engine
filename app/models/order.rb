# frozen_string_literal: true

if !Object.const_defined?('Order')
  class Order < ApplicationRecord
    has_many :order_styles, dependent: :destroy

    has_many :styles, through: :order_styles
  end
end
