# frozen_string_literal: true

require 'carrierwave'
require 'carrierwave/orm/activerecord'

class StylePhoto < ApplicationRecord
  belongs_to :brand, optional: true

  belongs_to :style

  mount_uploader :file, StylePhotoUploader
end
