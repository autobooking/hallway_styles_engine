# frozen_string_literal: true

if !Object.const_defined?('SeoBlock')
  class SeoBlock < ApplicationRecord
    belongs_to :style, optional: true
  end
end
