# frozen_string_literal: true

class OrderStyle < ApplicationRecord
  belongs_to :order

  belongs_to :style
end
