# frozen_string_literal: true

if !Object.const_defined?('Settings')
  class Settings < RailsSettings::Base
  end
end
