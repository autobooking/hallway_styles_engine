# frozen_string_literal: true

module AdminPanel
  class PermittedCountries
    attr_reader :object, :current_admin

    def initialize(object, current_admin)
      @object = object
      @current_admin = current_admin
    end

    def countries
      if current_admin.super_admin?
        CountrySetting.iso_codes
      else
        country_code ? object_permitted_countries : []
      end
    end

    private

    def country_code
      current_admin.country_setting&.country_code
    end

    def object_permitted_countries
      CountrySetting.iso_codes.map do |iso|
        if object.permitted_countries.include?(iso.last)
          iso.last != country_code ? iso << { disabled: true } : iso
        end
      end | CountrySetting.iso_codes.select { |iso| iso.last == country_code }
    end
  end
end
