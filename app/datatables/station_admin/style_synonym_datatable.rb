# frozen_string_literal: true

module StationAdmin
  class StyleSynonymDatatable < AjaxDatatablesRails::ActiveRecord
    extend Forwardable

    def_delegator :@view, :link_to
    def_delegator :@view, :edit_station_admin_style_synonym_path
    def_delegator :@view, :station_admin_style_synonym_path

    def initialize(params, options = {})
      @view = options[:view_context]
      @in_my_country = options[:in_my_country]
      @current_user = options[:current_user]
      super
    end

    def view_columns
      @view_columns ||= {
        id: { source: 'StyleSynonym.id' },
        name: { source: 'StyleSynonym::Translation.name' },
        in_my_country: { searchable: false, orderable: false },
        edit: { searchable: false, orderable: false }
      }
    end

    def data
      records.map do |record|
        {
          id: record.id,
          name: record.name,
          in_my_country: in_my_country(record),
          edit: edit(record)
        }
      end
    end

    def get_raw_records
      if @in_my_country
        StyleSynonym.eager_load(:translations).with_country(@current_user.country_code).all
      else
        StyleSynonym.eager_load(:translations).all
      end
    end

    private

    def in_my_country(record)
      record.permitted_countries.include?(@current_user.country_code) ? I18n.t('yes') : I18n.t('no')
    end

    def edit(record)
      link_to(I18n.t('datatables.station_admin.style_synonyms.edit', scope: :hallway_styles),
              edit_station_admin_style_synonym_path(record))
    end
  end
end
