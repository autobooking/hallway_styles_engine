# frozen_string_literal: true

module StationAdmin
  class StationStyleDatatable < AjaxDatatablesRails::ActiveRecord
    extend Forwardable

    def_delegator :@view, :link_to
    def_delegator :@view, :station_admin_station_station_style_path

    def initialize(params, options = {})
      @view = options[:view_context]
      @station = options[:station]
      super
    end

    def view_columns
      @view_columns ||= {
        id: { source: 'Style.id' },
        name: { source: 'Style.name' },
        delete: { searchable: false, orderable: false }
      }
    end

    def data
      records.map do |record|
        {
          id: record.id,
          name: record.name,
          delete: delete(record)
        }
      end
    end

    def get_raw_records
      @station.styles.includes(:translations).all
    end

    private

    def delete(record)
      link_to(I18n.t('datatables.station_admin.station_styles.remove', scope: :hallway_styles),
              station_admin_station_station_style_path(@station, record),
              method: :delete,
              data: { confirm: I18n.t('datatables.station_admin.station_styles.confirm_remove', scope: :hallway_styles) })
    end
  end
end
