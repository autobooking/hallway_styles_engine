# frozen_string_literal: true

require 'carrierwave'

class StylePhotoUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick

  storage :fog

  def store_dir
    "#{ Rails.env }/uploads/#{ model.class.to_s.underscore }/#{ model.id }/#{ mounted_as }"
  end

  version :thumb do
    process resize_to_fit: [200, 100]
  end

  version :medium do
    process resize_to_fit: [450, 450]
  end
end
