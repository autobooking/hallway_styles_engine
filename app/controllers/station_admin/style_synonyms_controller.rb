# frozen_string_literal: true

module StationAdmin
  class StyleSynonymsController < BaseController
    def index
      if current_user.operator?
        in_my_country = params.delete(:in_my_country)

        session[:in_my_country] = in_my_country

        respond_to do |format|
          format.html
          format.json do
            render json: StyleSynonymDatatable.new(params,
                                                   view_context: view_context,
                                                   in_my_country: in_my_country,
                                                   current_user: current_user)
          end
        end
      else
        redirect_to station_admin_root_url
      end
    end

    def edit
      @active_tab = 'styles'
      @style_synonym = StyleSynonym.find(params[:id])
      @styles = Style.includes(:translations) - @style_synonym.styles.includes(:translations)
    end

    def update
      @style_synonym = StyleSynonym.find(params[:id])

      @style_synonym.update!(resource_params)

      redirect_to edit_station_admin_style_synonym_path(@style_synonym)
    end

    private

    def resource_params
      params.require(:style_synonym).permit(:name).tap do |hash|
        hash[:translatable_columns_values] = params[:style_synonym][:translatable_columns_values].permit!
      end
    end
  end
end
