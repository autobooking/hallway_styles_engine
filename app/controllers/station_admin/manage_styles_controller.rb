# frozen_string_literal: true

module StationAdmin
  class ManageStylesController < BaseController
    def create
      @style_synonym = StyleSynonym.find(params[:style_synonym_id])
      @style = Style.find(params[:id])
      @style_synonym.styles << @style
      # @style_synonym.reindex
      redirect_to edit_station_admin_style_synonym_path(@style_synonym)
    end

    def destroy
      @style_synonym = StyleSynonym.find(params[:style_synonym_id])
      @style = Style.find(params[:id])
      @style_synonym.styles.destroy(@style)
      # @style_synonym.reindex
      redirect_to edit_station_admin_style_synonym_path(@style_synonym)
    end
  end
end
