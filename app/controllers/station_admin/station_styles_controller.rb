# frozen_string_literal: true

module StationAdmin
  class StationStylesController < BaseController
    def index
      @station = Station.find(params[:station_id])
      @active_tab = 'station_styles'
      params[:display_menu_for_station] = true
      @styles = Style.includes(:translations)
                     .with_country(current_user.country_code) - @station.styles.includes(:translations)

      respond_to do |format|
        format.html
        format.json do
          render json: StationStyleDatatable.new(params,
                                                 station: @station,
                                                 view_context: view_context)
        end
      end
    end

    def create
      @station = Station.find(params[:station_id])
      @style = Style.find(params[:id])
      @station.styles << @style
      # @station.reindex
      redirect_to station_admin_station_station_styles_path(@station)
    end

    def destroy
      @station = Station.find(params[:station_id])
      @style = Style.find(params[:id])
      @station.styles.destroy(@style)
      # @station.reindex
      redirect_to station_admin_station_station_styles_path(@station)
    end
  end
end
