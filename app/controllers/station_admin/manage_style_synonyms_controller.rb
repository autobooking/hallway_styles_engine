# frozen_string_literal: true

module StationAdmin
  class ManageStyleSynonymsController < BaseController
    def create
      @style = Style.find(params[:style_id])
      @style_synonym = StyleSynonym.find(params[:id])
      @style.style_synonyms << @style_synonym
      # @style.reindex
      redirect_to edit_station_admin_style_path(@style)
    end

    def destroy
      @style = Style.find(params[:style_id])
      @style_synonym = StyleSynonym.find(params[:id])
      @style.style_synonyms.destroy(@style_synonym)
      # @style.reindex
      redirect_to edit_station_admin_style_path(@style)
    end
  end
end
