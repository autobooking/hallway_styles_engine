# frozen_string_literal: true

module StationAdmin
  class StylesController < BaseController
    def index
      if current_user.operator?
        @active_tab = 'styles'

        in_my_country = params.delete(:in_my_country)

        session[:in_my_country] = in_my_country

        respond_to do |format|
          format.html
          format.json do
            render json: StyleDatatable.new(params,
                                            view_context: view_context,
                                            in_my_country: in_my_country,
                                            current_user: current_user)
          end
        end
      else
        redirect_to station_admin_root_url
      end
    end

    def edit
      @active_tab = 'styles'
      @style = Style.find(params[:id])
      @style_synonyms = StyleSynonym.includes(:translations) - @style.style_synonyms.includes(:translations)
    end

    def update
      @style = Style.find(params[:id])

      @style.update!(resource_params)

      update_permitted_countries

      redirect_to edit_station_admin_style_path(@style)
    end

    def destroy
      @style = Style.find(params[:id])

      @style.remove_country(current_user.country_code)

      redirect_to station_admin_styles_path(anchor: 'styles', in_my_country: session[:in_my_country])
    end

    private

    def resource_params
      params.require(:style).permit(:name, :slug, :tooltip, :description).tap do |hash|
        hash[:translatable_columns_values] = params[:style][:translatable_columns_values].permit!
      end
    end

    def update_permitted_countries
      if params[:in_my_country]
        @style.add_country(current_user.country_code)
      else
        @style.remove_country(current_user.country_code)
      end
    end
  end
end
