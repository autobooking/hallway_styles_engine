# frozen_string_literal: true

module Api
  class StationStylesController < ApiController
    def index
      station = Station.find(params[:station_id])

      respond_to do |format|
        format.json { render json: station.fetch_styles.as_json }
      end
    end
  end
end
