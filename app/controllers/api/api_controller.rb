# frozen_string_literal: true

if !Object.const_defined?('Api::ApiController')
  module Api
    class ApiController < ActionController::Base
    end
  end
end
