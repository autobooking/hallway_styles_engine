# frozen_string_literal: true

Settings.class_eval do
  field :use_styles, type: :boolean, default: false
end
