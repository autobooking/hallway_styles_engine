# frozen_string_literal: true

CountrySetting.class_eval do
  scope :with_default_code, -> { where(country_code: 'default') }

  def self.iso_codes
    if CountrySetting.with_default_code.any?
      default_code_option | ISO3166::Country.all_names_with_codes(I18n.locale)
    else
      ISO3166::Country.all_names_with_codes(I18n.locale)
    end
  end

  private

  def default_code_option
    [['Default', 'DEFAULT']]
  end
end
