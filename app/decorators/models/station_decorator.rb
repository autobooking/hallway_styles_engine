# frozen_string_literal: true

Station.class_eval do
  has_many :station_styles, dependent: :destroy

  has_many :styles, through: :station_styles

  # TODO: Translate `category_name`
  def fetch_styles
    station_styles.with_locale(I18n.locale)
                  .select('styles.id', 'style_translations.name')
                  .map { |style| { id: style.id, name: style.name, category_name: 'Styles' } }
  end

  def with_matched_style_names_by_id(style_ids)
    styles.where(id: style_ids).includes(:translations).map(&:name)
  end
end
