# frozen_string_literal: true

SeoBlock.class_eval do
  belongs_to :style, optional: true
end
