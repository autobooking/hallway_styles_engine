# frozen_string_literal: true

StationStyle.class_eval do
  scope :with_locale, ->(locale) { joins(style: :translations).where('style_translations.locale = ?', locale) }
end
