# frozen_string_literal: true

Order.class_eval do
  has_many :order_styles, dependent: :destroy

  has_many :styles, through: :order_styles
end
