# frozen_string_literal: true

Brand.class_eval do
  has_many :style_photos, dependent: :destroy
end
