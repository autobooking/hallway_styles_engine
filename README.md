# HallwayStyles

Short description and motivation.

## Usage

How to use my plugin.

## Installation

Add engine dependencies to Gemfile:

```ruby
gem 'coffee-script'
gem 'select2-rails'
gem 'rails-settings-cached'
gem 'activeadmin'
gem 'activeadmin_hstore_editor', git: 'https://github.com/wild5r/activeadmin_hstore_editor.git', branch: 'master'
gem 'activeadmin-select2', git: 'https://github.com/NechiK/activeadmin-select2.git', branch: 'master'
gem 'activeadmin_settings_cached'
gem 'globalize'
gem 'carrierwave'
gem 'ajax-datatables-rails'
gem 'countries'
gem 'babosa'
gem 'friendly_id'
gem 'friendly_id-globalize'
gem 'searchkick'
```

Add this line to your application's Gemfile:

```ruby
gem 'hallway_styles', git: 'git@gitlab.com:autobooking/hallway_styles_engine.git'
```

And then execute:

```bash
$ bundle
```

Install migrations:

```bash
$ bundle exec rails hallway_styles_engine:install:migrations
```

Add javascript to e.g. `application.js`:

```
//= require hallway_styles/station_admin/styles
//= require hallway_styles/station_admin/style_synonyms
//= require hallway_styles/station_admin/station_styles
```

Configure `ActiveadminSettingsCached`. Add `use_styles` to `config.display`:

```ruby
ActiveadminSettingsCached.configure do |config|
  config.display = {
    use_styles: 'boolean'
  }
end
```

Add `use_styles` to `config/app.yml`:

```yaml
defaults: &defaults
  use_styles: true

development:
  <<: *defaults

staging:
  <<: *defaults

production:
  <<: *defaults
```
