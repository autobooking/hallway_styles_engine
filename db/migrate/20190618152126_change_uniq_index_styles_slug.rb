# frozen_string_literal: true

class ChangeUniqIndexStylesSlug < ActiveRecord::Migration[5.2]
  def change
    remove_index :style_translations, column: :slug, unique: true
    add_index :style_translations, :slug
  end
end
