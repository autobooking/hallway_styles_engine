# frozen_string_literal: true

class CreateOrderStyles < ActiveRecord::Migration[5.2]
  def change
    create_join_table :orders, :styles
  end
end
