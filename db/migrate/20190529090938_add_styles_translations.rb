# frozen_string_literal: true

class AddStylesTranslations < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Style.create_translation_table!(name: :string,
                                        slug: :string,
                                        tooltip: :string,
                                        description: :text)
      end

      dir.down do
        Style.drop_translation_table!
      end
    end
  end
end
