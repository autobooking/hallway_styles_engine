# frozen_string_literal: true

class CreateStationStyles < ActiveRecord::Migration[5.2]
  def change
    create_table :station_styles do |t|
      t.bigint :station_id
      t.bigint :style_id

      t.timestamps

      t.index :station_id
      t.index :style_id

      t.index [:station_id, :style_id], unique: true
    end
  end
end
