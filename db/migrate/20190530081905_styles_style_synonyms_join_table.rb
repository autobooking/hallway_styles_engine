# frozen_string_literal: true

class StylesStyleSynonymsJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :styles, :style_synonyms
  end
end
