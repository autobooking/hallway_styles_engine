# frozen_string_literal: true

class AddIdToStyleStyleSynonyms < ActiveRecord::Migration[5.2]
  def change
    add_column :style_style_synonyms, :id, :primary_key
  end
end
