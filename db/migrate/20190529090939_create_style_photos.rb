# frozen_string_literal: true

class CreateStylePhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :style_photos do |t|
      t.references :style
      t.string :file
      t.text :description

      t.timestamps
    end
  end
end
