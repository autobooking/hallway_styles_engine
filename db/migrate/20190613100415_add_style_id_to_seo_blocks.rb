# frozen_string_literal: true

class AddStyleIdToSeoBlocks < ActiveRecord::Migration[5.2]
  def change
    add_column :seo_blocks, :style_id, :bigint

    add_index :seo_blocks, :style_id
  end
end
