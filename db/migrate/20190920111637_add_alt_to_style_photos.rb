# frozen_string_literal: true

class AddAltToStylePhotos < ActiveRecord::Migration[5.2]
  def change
    add_column :style_photos, :alt, :string
  end
end
