# frozen_string_literal: true

class AddUniqIndexStyleTranslationsSlug < ActiveRecord::Migration[5.2]
  def change
    add_index :style_translations, :slug, unique: true
  end
end
