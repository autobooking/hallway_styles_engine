# frozen_string_literal: true

class AddIndexToOrderStyles < ActiveRecord::Migration[5.2]
  def change
    add_index :orders_styles, [:order_id, :style_id], unique: true
  end
end
