# frozen_string_literal: true

class AddIdToOrderStyles < ActiveRecord::Migration[5.2]
  def change
    add_column :order_styles, :id, :primary_key
  end
end
