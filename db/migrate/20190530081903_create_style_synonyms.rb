# frozen_string_literal: true

class CreateStyleSynonyms < ActiveRecord::Migration[5.2]
  def change
    create_table :style_synonyms do |t|
      t.string :name
      t.text :permitted_countries, default: [], array: true

      t.timestamps
    end
  end
end
