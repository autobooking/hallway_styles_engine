# frozen_string_literal: true

class OrderStyles < ActiveRecord::Migration[5.2]
  def change
    rename_table :orders_styles, :order_styles
  end
end
