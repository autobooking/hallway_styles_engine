# frozen_string_literal: true

class RemoveStylePhotosDescription < ActiveRecord::Migration[5.2]
  def change
    remove_column :style_photos, :description, :text
  end
end
