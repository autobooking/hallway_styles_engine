# frozen_string_literal: true

class CreateStyles < ActiveRecord::Migration[5.2]
  def change
    create_table :styles do |t|
      t.string :name
      t.string :slug
      t.string :tooltip
      t.text :description
      t.text :permitted_countries, default: [], array: true
      t.boolean :main, default: false

      t.timestamps
    end
  end
end
