# frozen_string_literal: true

class AddStyleSynonymsTranslations < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        StyleSynonym.create_translation_table!(name: :string)
      end

      dir.down do
        StyleSynonym.drop_translation_table!
      end
    end
  end
end
