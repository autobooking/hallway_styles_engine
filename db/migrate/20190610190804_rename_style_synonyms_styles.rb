# frozen_string_literal: true

class RenameStyleSynonymsStyles < ActiveRecord::Migration[5.2]
  def change
    rename_table :style_synonyms_styles, :style_style_synonyms
  end
end
