# frozen_string_literal: true

class AddBrandToStylePhotos < ActiveRecord::Migration[5.2]
  def change
    add_reference(:style_photos, :brand)
  end
end
