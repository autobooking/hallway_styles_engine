# frozen_string_literal: true

require 'rails_helper'

describe StylePhoto do
  it { should be_an(ApplicationRecord) }

  it { should belong_to(:brand) }

  it { should belong_to(:style) }
end
