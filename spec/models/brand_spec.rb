# frozen_string_literal: true

require 'rails_helper'

describe Brand do
  it { should have_many(:style_photos).dependent(:destroy) }
end
