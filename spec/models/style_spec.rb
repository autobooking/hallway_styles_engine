# frozen_string_literal: true

require 'rails_helper'

describe Style do
  it { should be_an(ApplicationRecord) }

  it { should have_many(:style_photos).dependent(:destroy) }

  it { should have_and_belong_to_many(:style_synonyms) }
end
