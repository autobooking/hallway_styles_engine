# frozen_string_literal: true

$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'hallway_styles/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'hallway_styles'
  spec.version     = HallwayStyles::VERSION
  spec.authors     = ['Igor Zubkov']
  spec.email       = ['igor.zubkov@gmail.com']
  spec.homepage    = 'https://gitlab.com/autobooking/hallway_styles_engine/'
  spec.summary     = 'Hallway: styles module'
  spec.description = spec.summary

  spec.files = Dir['{app,config,db,lib}/**/*', 'Rakefile', 'README.md']

  spec.add_runtime_dependency 'rails'
  spec.add_runtime_dependency 'coffee-script'
  spec.add_runtime_dependency 'rails-settings-cached'
  spec.add_runtime_dependency 'activeadmin'
  spec.add_runtime_dependency 'activeadmin_json_editor'
  spec.add_runtime_dependency 'activeadmin_addons'
  spec.add_runtime_dependency 'activeadmin_settings_cached'
  spec.add_runtime_dependency 'globalize'
  spec.add_runtime_dependency 'carrierwave'
  spec.add_runtime_dependency 'ajax-datatables-rails'
  spec.add_runtime_dependency 'countries'
  spec.add_runtime_dependency 'babosa'
  spec.add_runtime_dependency 'friendly_id'
  spec.add_runtime_dependency 'friendly_id-globalize'
  spec.add_runtime_dependency 'searchkick'

  spec.add_development_dependency 'pg'
  spec.add_development_dependency 'rspec-rails'
  spec.add_development_dependency 'shoulda-matchers'
end
